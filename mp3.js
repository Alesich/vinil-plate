const button = document.getElementById('button'),
    plate = document.getElementById('plate'),
    on = document.getElementById('on'),
    off = document.getElementById('off'),
    thing = document.getElementById('thing'),
    timer = document.getElementById('timer'),
    songs = document.getElementById('songs'),
    plateTitle = document.getElementById('plate-title'),
    plates = document.querySelectorAll('.plate'),
    track = document.querySelectorAll('audio');

let state = false;
let ms = '0' + 0,
    seconds = '0' + 0,
    min = '0' + 0,
    hours = '0' + 0;

function stopSong(song) {
    song.pause();
    song.currentTime = 0;
}

function music() {
    timeGo();
    plates.forEach((item, i) => {
        if (item.style.zIndex === '20') {
            track[i].play();
            if (!track[i]){
                zeroing();
            }
        }
    });
}

button.addEventListener('click', () => {
    if (!state) {
        start();

    }   else if (state) {
        rotate();
        plates.forEach((item, i) => {
            if (item.style.zIndex === '20') {
                track[i].pause();
            }
        })
    }

})

function timeGo() {

    const timerId = setInterval(() => {
        if (state) {
            ms++;

            if (ms < 10) {
                ms = '0' + ms;
            } else if (ms > 10) {
                ms = '0' + 0;
                seconds++;

                if (seconds < 10) {
                    seconds = "0" + seconds;
                } else if (seconds > 60) {
                    seconds = "0" + 0;
                    min++;

                    if (min < 10) {
                        min = "0" + min;
                    } else if (min > 60) {
                        min = "0" + 0;
                        hours++;

                        if (hours < 10) {
                            hours = "0" + hours;
                        }
                    }
                }
            }
            timer.innerText = hours + ':' + min + ':' + seconds + ':' + ms;
        } else if (!state) {
            clearInterval(timerId);
        }
    }, 100);
}


songs.addEventListener('click', (e) => {
    for (let i = 0; i < plates.length; i++) {
        plates[i].style.zIndex = '0';
        rotate();
        plate.classList.remove("spin");
        plateTitle.classList.remove('spin');
        zeroing();

        plates.forEach((item, i) => {
            if (plates[i].style.zIndex === '20') {
                stopSong(track[i]);
            }
        })
    }
    if (e.target.classList.contains("box") ) {
        e.target.nextElementSibling.style.zIndex = '20';
        plateTitle.innerText = e.target.nextElementSibling.nextElementSibling.innerText;
    }
})

function zeroing() {
    ms = '0' + 0,
        seconds = '0' + 0,
        min = '0' + 0,
        hours = '0' + 0;
    timer.innerText = '00:00:00:00';
}

function rotate() {
    plateTitle.classList.toggle('spin');
    plate.classList.toggle("spin");
    state = false;
    thing.style.transform = 'rotate(0deg)';
    off.style.display = 'inline-block';
    on.style.display = 'none';
}

function start() {
    plateTitle.classList.toggle('spin');
    plate.classList.toggle("spin");
    state = true;
    thing.style.transform = 'rotate(20deg)';
    on.style.display = 'inline-block';
    off.style.display = 'none';
    const timerDelay = setTimeout(music, 1000);
}

track.forEach((item) => {
    item.addEventListener('ended', () => {
        rotate();
        zeroing();
    })
})